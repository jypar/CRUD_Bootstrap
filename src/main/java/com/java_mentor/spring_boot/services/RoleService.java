package com.java_mentor.spring_boot.services;

import com.java_mentor.spring_boot.entities.Role;

import java.util.List;

public interface RoleService {

    List<Role> getRolesList();
}
